module adder_tb;
  logic[31:0] A_in;
  logic[31:0] B_in;
  wire[31:0] O_out;
  
adder L1(
          .A_in(A_in)
         ,.B_in(B_in)
         ,.O_out(O_out)
         );

initial begin
/*arithmetic operation*/

	   A_in = 32'b00000000000000000000000000000000;
	   B_in = 32'b00000000000000000000000000000001;
	   #10;

	   A_in = 32'b00000000000000000000000000000010;
	   B_in = 32'b00000000000000000000000000000001;
	   #10;
	  
	   A_in = 32'b00000000000000000000000000000011;
	   B_in = 32'b00000000000000000000000000000001;
	   #10;
end
endmodule
    
