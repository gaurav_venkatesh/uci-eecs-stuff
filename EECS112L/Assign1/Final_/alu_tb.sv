module alu_tb;
	logic[31:0] A_in;
	logic[31:0] B_in;
	logic[3:0] Func_in;
	wire[31:0] O_out;
  
alu L1(
.Func_in(Func_in),
.A_in(A_in),
.B_in(B_in),
.O_out(O_out)
);

initial begin
/*round 1*/
A_in = 32'b11111111111111111111111111111011;
B_in = 32'b00000000000000000000000000000001;
Func_in = 4'b0000;	/*AND*/
#10;     
Func_in = 4'b0001;	/*OR*/
#10;
Func_in = 4'b0010;	/*ADD signed*/
#10;
Func_in = 4'b0110;	/*SUB signed*/
#10;
Func_in = 4'b0111;	/*SLT signed*/
#10
Func_in = 4'b1100;	/*NOR*/
#10;

/*round 2*/
A_in = 32'b10000000000000000000000000000000;
B_in = 32'b00000000000000000000000000000001;
Func_in = 4'b0000;	/*AND*/
#10;     
Func_in = 4'b0001;	/*OR*/
#10;
Func_in = 4'b0010;	/*ADD signed*/
#10;
Func_in = 4'b0110;	/*SUB signed*/
#10;
Func_in = 4'b0111;	/*SLT signed*/
#10
Func_in = 4'b1100;	/*NOR*/
#10;

/*round 3*/
A_in = 32'b00000000000000000000000000000000;
B_in = 32'b11111111111111111111111111111111;
Func_in = 4'b0000;	/*AND*/
#10;     
Func_in = 4'b0001;	/*OR*/
#10;
Func_in = 4'b0010;	/*ADD signed*/
#10;
Func_in = 4'b0110;	/*SUB signed*/
#10;
Func_in = 4'b0111;	/*SLT signed*/
#10
Func_in = 4'b1100;	/*NOR*/
#10;

end
endmodule
    
