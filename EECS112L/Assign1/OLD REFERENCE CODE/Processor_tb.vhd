LIBRARY IEEE;
USE ieee.std_logic_1164.ALL; 
USE work.c31L_pack.all; 
---------------------------------------------------------
ENTITY processor_TB IS
END processor_TB;
----------------------------------------------------------
ARCHITECTURE TEST of processor_TB IS
COMPONENT processor
PORT(
	clk : in std_logic;
	instruction : in std_logic_vector(BW-1 downto 0);
	over_flow : out std_logic;
	carry : out std_logic
);
END COMPONENT processor;

SIGNAL clk_tb : std_logic;
SIGNAL instruction_tb : std_logic_vector(BW-1 downto 0);
SIGNAL over_flow_tb : std_logic;
SIGNAL carry_tb : std_logic;

BEGIN
	processor_INST : processor PORT MAP(clk_tb, instruction_tb, over_flow_tb, carry_tb);

	clock: PROCESS

	BEGIN 
	clk_tb <= '0';
	wait for 10 ns;
	clk_tb <= '1';
	wait for 10 ns;
	END PROCESS clock;

	CPUBody: PROCESS
	BEGIN

	WAIT FOR 10 ns;

	instruction_tb <= "10101011110010001000111011111111"; --ADD
	WAIT FOR 10 ns;

	instruction_tb <= "00001010011010110001101000000111"; --OR
	WAIT FOR 10 ns;

	instruction_tb <= "00000110000010111000101000000010"; --NOT
	WAIT FOR 10 ns;

	instruction_tb <= "00000010000111000001000000110101"; --XOR
	WAIT FOR 10 ns;

	instruction_tb <= "00110110001110010000001111111111"; --SUB
	WAIT FOR 10 ns;

	instruction_tb <= "10101011110011001000000000000001"; --SLL 
	WAIT FOR 10 ns;

	instruction_tb <= "00101111111110101000101000000101"; --AND
	WAIT FOR 10 ns;

	instruction_tb <= "10101001100000000000000000000011"; --NOP
	WAIT FOR 10 ns;

	instruction_tb <= "10001110000111011000011000000111"; --MOVI
	WAIT FOR 10 ns;

	instruction_tb <= "00001010001111011000010001100101"; --MOV
	WAIT FOR 10 ns;

	END PROCESS CPUBody;
	assert false report "finish" severity failure;
END TEST;
	
	