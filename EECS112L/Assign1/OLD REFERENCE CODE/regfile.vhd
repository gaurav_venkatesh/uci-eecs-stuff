library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 
USE work.c31L_pack.all; 
------------------------------------------------------------------
ENTITY reg_bank IS

PORT (
	clk : IN std_logic ;
	re : IN std_logic; --read enable
	we : IN std_logic ; -- write enable
	rs_index : in std_logic_vector(reg_field-1 downto 0); --read address1
	rt_index : in std_logic_vector(reg_field-1 downto 0); --read address2
	rd_index : in std_logic_vector(reg_field-1 downto 0); --write address
	reg_source_out : out std_logic_vector(BW-1 downto 0); --read data1
	reg_target_out : out std_logic_vector(BW-1 downto 0); -- read data2
	reg_dest_new : in std_logic_vector(BW-1 downto 0) --write data
);
END reg_bank ;
---------------------------------------------------------------------
ARCHITECTURE behave OF reg_bank IS
SUBTYPE cell is std_logic_vector(BW -1 DOWNTO 0);
TYPE regfile is array ((2**reg_field)-1 DOWNTO 0) of cell;
BEGIN
----------------------------------------------------------------------
PROCESS(we, re)
VARIABLE y : regfile := ("00000000001111111111000001111101", "00000000000000111111111100000011", 
			"11111111000001010101111100001000", "00001111000011110001011001110001", 
			OTHERS => (OTHERS => '0'));
BEGIN
	IF (clk = '0') THEN
			y(to_integer(unsigned(rd_index))) := reg_dest_new; 

	ELSIF (clk = '1') THEN 
		reg_source_out  <=  y(TO_INTEGER(UNSIGNED(rs_index)));
		reg_target_out  <=  y(TO_INTEGER(UNSIGNED(rt_index)));


	END IF;

END PROCESS;
---------------------------------------------------------------
END behave;