library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE work.c31L_pack.all;  
------------------------------------------------------------------
entity processor is
port(
	clk : in std_logic;
	instruction : in std_logic_vector(BW-1 downto 0);
	over_flow : out std_logic;
	carry : out std_logic
);
end processor;
------------------------------------------------------------
ARCHITECTURE bbeehh OF processor IS

COMPONENT reg_bank
PORT(
	clk : IN std_logic ;
	re : IN std_logic; --read enable
	we : IN std_logic ; -- write enable
	rs_index : in std_logic_vector(reg_field-1 downto 0); --read address1
	rt_index : in std_logic_vector(reg_field-1 downto 0); --read address2
	rd_index : in std_logic_vector(reg_field-1 downto 0); --write address
	reg_source_out : out std_logic_vector(BW-1 downto 0); --read data1
	reg_target_out : out std_logic_vector(BW-1 downto 0); -- read data2
	reg_dest_new : in std_logic_vector(BW-1 downto 0) --write data
);
END COMPONENT;

COMPONENT mux2to1
PORT(
	in0 : IN std_logic_vector(31 downto 0);
	in1 : IN std_logic_vector(31 downto 0);
	mode : IN std_logic ;
	output : OUT std_logic_vector(31 downto 0)
);
END COMPONENT;

COMPONENT control 
PORT(
	clk : in std_logic;
	instruction : in std_logic_vector(BW-1 downto 0);
	rt_and_imm : out std_logic_vector (31 downto 0);
	re : out std_logic;
	we : out std_logic;
	rs_index : out std_logic_vector(reg_field-1 downto 0);
	rt_index : out std_logic_vector(reg_field-1 downto 0);
	rd_index : out std_logic_vector(reg_field-1 downto 0);
	b_mux_sel : out std_logic;
	alu_func : out alu_function_type
);
END COMPONENT;

COMPONENT alu32
PORT(
	a_alu32 : in std_logic_vector(BW-1 downto 0);
	b_alu32 : in std_logic_vector(BW-1 downto 0);
	alu_op : in alu_function_type;
	bmux_sel : in std_logic; 
	o_alu32 : out std_logic_vector(BW-1 downto 0);
	ov_alu32 : out std_logic;
	c_alu32 : out std_logic
);
END COMPONENT;
-----------------------------------------------------------
SIGNAL s1 : std_logic_vector(31 downto 0);
SIGNAL s2 : std_logic_vector(5 downto 0);
SIGNAL s3 : std_logic_vector(5 downto 0);
SIGNAL s4 : std_logic_vector(5 downto 0);
SIGNAL s5 : std_logic;
SIGNAL s6 : std_logic;
SIGNAL s7 : std_logic_vector(31 downto 0);
SIGNAL s8 : std_logic_vector(31 downto 0);
SIGNAL s9 : std_logic;
SIGNAL s10 : std_logic_vector(31 downto 0); 
SIGNAL s11 : std_logic_vector(3 downto 0);
SIGNAL s12 : std_logic_vector(31 downto 0);

BEGIN
	component1: control PORT MAP(clk, instruction, s1, s5, s6, s2, s4, s3, s9, s11);
	component2: reg_bank PORT MAP(clk, s5, s6, s2, s4, s3, s8, s7, s12);
	component3: mux2to1 PORT MAP(s7, s1, s9, s10);
	component4: alu32 PORT MAP(s8, s10, s11, s9, s12, over_flow, carry);

END bbeehh;
