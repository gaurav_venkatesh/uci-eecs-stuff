library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 
USE work.c31L_pack.all; 
--------------------------------------------------------------------
entity control is

port(
	clk : in std_logic;
	instruction : in std_logic_vector(BW-1 downto 0);
	rt_and_imm : out std_logic_vector (31 downto 0);
	re : out std_logic;
	we : out std_logic;
	rs_index : out std_logic_vector(reg_field-1 downto 0);
	rt_index : out std_logic_vector(reg_field-1 downto 0);
	rd_index : out std_logic_vector(reg_field-1 downto 0);
	b_mux_sel : out std_logic;
	alu_func : out alu_function_type
);
end control; 
-------------------------------------------------------------------
ARCHITECTURE behh OF control IS
BEGIN
		rt_and_imm <= (instruction(14) & instruction(14) & instruction(14) & instruction(14) & 
		instruction(14) & instruction(14) & instruction(14) & instruction(14) & instruction(14) & 
		instruction(14) & instruction(14) & instruction(14) & instruction(14) & instruction(14) & 
		instruction(14) & instruction(14) & instruction(14) & instruction(14 downto 0));
		b_mux_sel <= instruction(31);
		rs_index <= instruction(30 downto 25);
		rd_index <= instruction(24 downto 19);
		alu_func <= instruction(18 downto 15);
		rt_index <= instruction(14 downto 9);

PROCESS(clk)
BEGIN 

		IF (clk'event AND clk = '0') THEN
			re <= '0';
			we <= '1';
		ELSIF (clk'event AND clk = '1') THEN
			we <= '0';
			re <= '1';
		ELSIF (instruction(18 downto 15) = "0000") THEN
			we <= '0';
		END IF;
END PROCESS;
END behh; 
