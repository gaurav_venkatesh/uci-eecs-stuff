library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 
USE work.c31L_pack.all;  
--------------------------------------------------------------------
entity alu32 is
port(
	a_alu32 : in std_logic_vector(BW-1 downto 0);
	b_alu32 : in std_logic_vector(BW-1 downto 0);
	alu_op : in alu_function_type;
	bmux_sel : in std_logic;
	o_alu32 : out std_logic_vector(BW-1 downto 0);
	ov_alu32 : out std_logic;
	c_alu32 : out std_logic
);
end alu32;
------------------------------------------------------------------
ARCHITECTURE behavioral OF alu32 IS

SIGNAL adding, subtracting : std_logic_vector(32 downto 0);
SIGNAL Atemp, Btemp : std_logic_vector(32 downto 0);
SIGNAL CtempAdd :  std_logic;
SIGNAL CtempSub :  std_logic;
SIGNAL move : std_logic_vector (31 DOWNTO 0);
SIGNAL x : std_logic;

BEGIN
	Atemp <= (a_alu32(31) & '0' & a_alu32(30 downto 0));
	Btemp <= (b_alu32(31) & '0' & b_alu32(30 downto 0));

	adding <= std_logic_vector((signed(Atemp)) + (signed(Btemp)));
	CtempAdd <= adding(32);

	subtracting <= std_logic_vector((signed(Atemp)) - (signed(Btemp)));
	CtempSub <= subtracting(32);


Process(alu_op)
BEGIN
	IF (alu_op = alu_add) THEN
		x <= CtempAdd;

	ELSIF (alu_op = alu_sub) THEN
		x <= CtempSub;
	END IF;
ov_alu32 <= x;
c_alu32 <= x;

END PROCESS;


PROCESS(alu_op, bmux_sel)
BEGIN	
	IF(bmux_sel = '0') THEN
	move <= a_alu32;
	ELSIF(bmux_sel = '1') THEN
	move <= b_alu32;
	END IF;

END PROCESS;

WITH alu_op SELECT
	
		o_alu32 <= (OTHERS => '0') WHEN alu_nop,
			   (adding(31 DOWNTO 0)) WHEN alu_add,
			   (subtracting(31 DOWNTO 0)) WHEN alu_sub,
			   (a_alu32 AND b_alu32) WHEN alu_and,
			   (a_alu32 OR b_alu32) WHEN alu_or,
			   (NOT a_alu32) WHEN alu_not,
			   (a_alu32 XOR b_alu32) WHEN alu_xor,
			   (std_logic_vector(signed(a_alu32) SLL TO_INTEGER(signed(b_alu32)))) WHEN alu_SLL,
			   move WHEN alu_mov,
			   (OTHERS => 'Z') WHEN OTHERS;

END behavioral;
