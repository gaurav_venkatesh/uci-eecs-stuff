library ieee;
use ieee.std_logic_1164.all;
use work.constants.all;		-- There's errors when the generics are used, so the constants are in a package.

ENTITY regfile IS
--	GENERIC ( NBIT : INTEGER := 32;
--		NSEL : INTEGER := 3);
	
	PORT (clk 	: IN std_logic;
		rst_s 	: IN std_logic; -- synchronous reset
		we 	: IN std_logic; -- write enable
		raddr_1 : IN std_logic_vector (NSEL -1 DOWNTO 0); -- read address 1
		raddr_2 : IN std_logic_vector (NSEL -1 DOWNTO 0); -- read address 2
		rdata_1 : OUT std_logic_vector (NBIT -1 DOWNTO 0); -- read data 1
		rdata_2 : OUT std_logic_vector (NBIT -1 DOWNTO 0); -- read data 2
		waddr 	: IN std_logic_vector (NSEL -1 DOWNTO 0); -- write address
		wdata 	: IN std_logic_vector (NBIT -1 DOWNTO 0) -- write data
		);
END regfile ;

ARCHITECTURE r_e_g_i_s_t_e_r of regfile is
	SIGNAL reg1, reg2, reg3, reg4, reg5, reg6, reg7, reg8 : std_logic_vector(NBIT - 1 DOWNTO 0) := (OTHERS => '0'); -- individual registers
BEGIN
	registry: PROCESS(clk, raddr_1, raddr_2, waddr, reg1, reg2, reg3, reg4, reg5, reg6, reg7, reg8)
	BEGIN
		CASE raddr_1 is					-- Outputting the data of the register whose addresses are entered.
			when "001" => rdata_1 <= reg1;
			when "010" => rdata_1 <= reg2;
			when "011" => rdata_1 <= reg3;
			when "100" => rdata_1 <= reg4;
			when "101" => rdata_1 <= reg5;
			when "110" => rdata_1 <= reg6;
			when "111" => rdata_1 <= reg7;
			when OTHERS => rdata_1 <= ZERO;
		END CASE;
		
		CASE raddr_2 is
			when "001" => rdata_2 <= reg1;
			when "010" => rdata_2 <= reg2;
			when "011" => rdata_2 <= reg3;
			when "100" => rdata_2 <= reg4;
			when "101" => rdata_2 <= reg5;
			when "110" => rdata_2 <= reg6;
			when "111" => rdata_2 <= reg7;
			when OTHERS => rdata_2 <= ZERO;
		END CASE;
		
		if (rising_edge(clk) and rst_s = '1') then	-- Synchronous reset
			reg1 <= ZERO;
			reg2 <= ZERO;
			reg3 <= ZERO;
			reg4 <= ZERO;
			reg5 <= ZERO;
			reg6 <= ZERO;
			reg7 <= ZERO;
		elsif (rising_edge(clk) and we = '1') then	-- Conditions to write to register
			CASE waddr is
				when "001" => reg1 <= wdata;
				when "010" => reg2 <= wdata;
				when "011" => reg3 <= wdata;
				when "100" => reg4 <= wdata;
				when "101" => reg5 <= wdata;
				when "110" => reg6 <= wdata;
				when "111" => reg7 <= wdata;
				when OTHERS => null;
			END CASE;
		END IF;
	END PROCESS;
END;