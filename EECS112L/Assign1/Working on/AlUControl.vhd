library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ALUControl is

port(
			clk: 			in std_logic;
			instruction:	in std_logic_vector(5 downto 0);
			ALUOP0:			in std_logic;
			ALUOP1:			in std_logic;
			Operation:		out std_logic_vector(3 downto 0);
);
end ALUControl;

ARCHITECTURE beh OF ALUControl IS
BEGIN
			IF(ALUOP0 ='0' & ALUOP1 = '0' & instruction ="XXXXXX") -- Check"" or ''
				Operation 	<=	0010;
			ElSIF(ALUOP0 ='1' & ALUOP1 = 'X' & instruction ="XXXXXX")
				Operation	<=  0110;
			ElSIF(ALUOP0 ='X' & ALUOP1 = '1' & instruction ="XX0000")
				Operation	<=	0010;
			ElSIF(ALUOP0 ='X' & ALUOP1 = '1' & instruction ="XX0010")
				Operation	<=	0110;
			ElSIF(ALUOP0 ='X' & ALUOP1 = '1' & instruction ="XX0100")
				Operation	<=	0000;
			ElSIF(ALUOP0 ='X' & ALUOP1 = '1' & instruction ="XX0101")
				Operation	<=	0001;
			ElSIF(ALUOP0 ='X' & ALUOP1 = '1' & instruction ="XX1010")
				Operation	<=	0111;
			END IF;
END beh;