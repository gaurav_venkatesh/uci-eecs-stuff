library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY Processor IS
port(
	ref_clk :		 out std_logic;
	instruction :	 in std_logic_vector();
	reset: 			 out std_logic;
);

ARCHITECTURE beh OF Processor IS 


COMPONENT regfile

PORT (
		clk 	: IN std_logic;
		rst_s 	: IN std_logic; -- synchronous reset
		we 		: IN std_logic; -- write enable
		raddr_1 : IN std_logic_vector (31 DOWNTO 0); -- read address 1
		raddr_2 : IN std_logic_vector (31 DOWNTO 0); -- read address 2
		rdata_1 : OUT std_logic_vector (31 DOWNTO 0); -- read data 1
		rdata_2 : OUT std_logic_vector (31 DOWNTO 0); -- read data 2
		waddr 	: IN std_logic_vector (31 DOWNTO 0); -- write address
		wdata 	: IN std_logic_vector (31 DOWNTO 0) -- write data
		);
END COMPONENT;

COMPONENT alu
PORT (
		Func_in	 	: IN std_logic_vector (5 DOWNTO 0);
		A_in 	 	: IN std_logic_vector (31 DOWNTO 0);
		B_in 	 	: IN std_logic_vector (31 DOWNTO 0);
		O_out 	 	: OUT std_logic_vector (31 DOWNTO 0);
		Branch_out	: OUT std_logic ;
		Jump_out 	: OUT std_logic 
); 
END COMPONENT;


COMPONENT control
PORT(
		clk : 			in std_logic;
		instruction : 	in std_logic_vector(5 downto 0);
		MemToReg:		out std_logic;
		MemWrite:		out std_logic;
		Branch:			out std_logic;
		ALUOP0:			out std_logic;
		ALUOP1:			out std_logic;
		ALUSrc:			out std_logic;
		RegDst:			out std_logic;
		RegWrite:		out std_logic
); 
END COMPONENT;


COMPONENT ram 
 PORT (	
		clk 	: IN std_logic;
		we 		: IN std_logic;
		addr 	: IN std_logic_vector (31 DOWNTO 0);
		dataI 	: IN std_logic_vector (31 DOWNTO 0);
		dataO 	: OUT std_logic_vector (31 DOWNTO 0)
);


END COMPONENT;

COMPONENT mux2to1
PORT (
		in0 : IN std_logic_vector(31 downto 0);
		in1 : IN std_logic_vector(31 downto 0);
		mode : IN std_logic ;
		output1 : OUT std_logic_vector(31 downto 0)
);
END COMPONENT;

COMPONENT mux2to1_5
PORT (
		in0 : IN std_logic_vector(31 downto 0);
		in1 : IN std_logic_vector(31 downto 0);
		mode : IN std_logic ;
		output1 : OUT std_logic_vector(31 downto 0)
);
END COMPONENT;


COMPONENT Sign_extend
PORT(
    		input1    	 : in 	std_logic_vector(15 downto 0);
    		output1   	 : out	std_logic_vector(31 downto 0)
);
END COMPONENT;

COMPONENT alu_control
PORT(
			clk: 			in std_logic;
			instruction:	in std_logic_vector(5 downto 0);
			ALUOP0:			in std_logic;
			ALUOP1:			in std_logic;
			Operation:		out std_logic_vector(3 downto 0)
);
END COMPONENT;
--From intruction mem to other parts
SIGNAL s1 : std_logic_vector(31 downto 26);
SIGNAL s2 : std_logic_vector(25 downto 21);
SIGNAL s3 : std_logic_vector(20 downto 16);
SIGNAL s4 : std_logic_vector(15 downto 11);
SIGNAL s5 : std_logic_vector(15 downto 0);
SIGNAL s6 : std_logic_vector(5 downto 0);

-- From Control to other parts
SIGNAL a1 : std_logic; -- MemtoReg
SIGNAL a2 : std_logic; -- MemWrite
SIGNAL a3 : std_logic; -- Branch
SIGNAL a4 : std_logic; -- ALUOP1
SIGNAL a5 : std_logic; -- ALUOP2
SIGNAL a6 : std_logic; -- RegWrite
SIGNAL a7 : std_logic; -- ALUSrc
SIGNAL a8 : std_logic; -- RegDst

-- From Sign_extend to other parts
SIGNAL b1 : std_logic_vector(31 downto 0);

-- From Register to other parts
SIGNAL c1 : std_logic_vector(31 downto 0);
SIGNAL c2 : std_logic_vector(31 downto 0);

--From mux 5 bit to otehr parts
SIGNAL d1 : std_logic_vector( 4 downto 0);

--From mux near ALU to other parts.
SIGNAL e1 : std_logic_vector(31 downto 0);

-- From ALU to other parts 
SIGNAL f1 : std_logic_vector(31 downto 0);

-- From Data memory to other parts
SIGNAL g1 : std_logic_vector(31 downto 0);

-- From Second Mux near the RAM to other parts
SIGNAL h1 : std_logic_vector(31 downto 0);

-- From ALU control
SIGNAL i1 : std_logic_vector(4 downto 0); 

BEGIN
	COMPONENT1: regfile 	PORT MAP( clk, rst_s, a6, s2, s3, c1, c2, d1, h1);
	COMPONENT2: alu			PORT MAP( clk, c1, e1, f1, Branch_out, Jump_out);
	COMPONENT3: control 	PORT MAP( clk, s1, a1, a2, a3, a4, a5, a7, a8, a6);
	COMPONENT4: ram			PORT MAP( clk, a2, f1, c2, g1);
	COMPONENT5: mux2to1 	PORT MAP( c2, b1, a7, e1);
	COMPONENT6: Sign_extend PORT MAP( s5, b1);
	COMPONENT7: mux2to1_5 	PORT MAP( s3, s4, a8, d1);
	COMPONENT8: mux2to1		PORT MAP( f1, g1, a1, h1);
	COMPONENT9: alu_control PORT MAP( clk, instruction, a4, a5, i1);
END beh;
