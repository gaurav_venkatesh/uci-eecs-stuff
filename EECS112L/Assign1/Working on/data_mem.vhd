library ieee;
use ieee.std_logic_1164.all;	-- Allan was here
use ieee.numeric_std.all;		-- Changed from word-addressable to byte-addressable

ENTITY ram IS
	port (clk	: IN std_logic;
		we 		: IN std_logic;
		addr 	: IN std_logic_vector (31 DOWNTO 0);
		dataI 	: IN std_logic_vector (31 DOWNTO 0);
		dataO 	: OUT std_logic_vector (31 DOWNTO 0));
END ram;

ARCHITECTURE mem OF ram IS
	TYPE memory IS array (2047 DOWNTO 0) of std_logic_vector (7 DOWNTO 0);
	SIGNAL bleh : memory := (OTHERS => (OTHERS => '0'));
	SIGNAL mario : integer range 0 to 2047;
BEGIN
	data: PROCESS (clk, we, addr, dataI)
	BEGIN
		mario <= to_integer(unsigned(addr(10 DOWNTO 0)));
		dataO <= bleh(mario) & 
			bleh(mario + 1) & 
			bleh(mario + 2) & 
			bleh(mario + 3);

		IF (rising_edge(clk) and we = '1')
		THEN
			bleh(mario) 	<= dataI (31 DOWNTO 24);
			bleh(mario + 1) <= dataI (23 DOWNTO 16);
			bleh(mario + 2) <= dataI (15 DOWNTO 8);
			bleh(mario + 3) <= dataI (7 DOWNTO 0);
		END IF;
	END PROCESS;
END;
