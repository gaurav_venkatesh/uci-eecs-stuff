library ieee;
USE ieee.std_logic_1164.ALL;

ENTITY clock IS
PORT(
	CLK : out std_logic
);
END clock;

ARCHITECTURE beh OF clock IS
BEGIN
	PROCESS
	BEGIN
	
		CLK <=  '0';
		wait for 50 ns;
		CLK <= '1';
		wait for 50 ns;
	
	END PROCESS;

END beh;
