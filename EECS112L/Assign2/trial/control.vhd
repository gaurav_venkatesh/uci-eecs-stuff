library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 
--USE work.c31L_pack.all; 

ENTITY control IS

port(
		clk : 			in std_logic;
		instruction : 	in std_logic_vector(5 downto 0);
		RegDst:			out std_logic;
		Branch:			out std_logic;
		MemRead:		out std_logic;
		MemToReg:		out std_logic;
		ALUOP0:			out std_logic;
		ALUOP1:			out std_logic;
		MemWrite:		out std_logic;
		ALUSrc:			out std_logic;
		RegWrite:		out std_logic;
		JumpMode:		out std_logic
);
end control;

ARCHITECTURE beh OF control IS 
BEGIN
		PROCESS (clk, instruction) IS
		BEGIN
			IF(instruction = "000000") THEN	-- Rtype
				ALUSrc 		<= '0';
				MemToReg	<= '0';
				RegDst		<= '1';
				RegWrite	<= '1';
				Branch		<= '0';
				MemWrite	<= '0';
				ALUOP0 		<= '1';
				ALUOP1		<= '0';
				MemRead		<= '0';
				JumpMode	<= '';
			ELSIF (instruction = "100011") THEN -- Load
				ALUSrc 		<= '1';
				MemToReg	<= '1';
				RegDst		<= '0';
				RegWrite	<= '1';
				Branch		<= '0';
				MemWrite	<= '0';
				ALUOP0		<= '0';
				ALUOP1		<= '0';
				MemRead		<= '1';
				JumpMode	<= '';
			ELSIF (instruction = "101011") THEN -- Store
				ALUSrc 		<= '1';
				MemToReg	<= 'X';
				RegDst		<= 'X';
				RegWrite	<= '0';	
				Branch		<= '0';
				MemWrite	<= '1';
				ALUOP0		<= '0';
				ALUOP1		<= '0';
				MemRead		<= '0';
				JumpMode	<= '';
			ELSIF (instruction = "000100") THEN -- Branch
				ALUSrc 		<= '0';
				MemToReg	<= 'X';
				RegDst		<= 'X';
				RegWrite	<= '0';	
				Branch		<= '1';
				MemWrite	<= '0';
				ALUOP0		<= '0';
				ALUOP1		<= '1';
				MemRead		<= '0';
				JumpMode	<= '0';
			ELSIF (instruction = "000101") THEN -- Branch Not Equal
				ALUSrc 		<= '0';
				MemToReg	<= 'X';
				RegDst		<= 'X';
				RegWrite	<= '0';	
				Branch		<= '0';
				MemWrite	<= '0';
				ALUOP0		<= '0';
				ALUOP1		<= '1';
				MemRead		<= '0';
				JumpMode	<= '0';
			ELSIF (instruction = "0000001") THEN -- BLTZ Branch on less than zero.
			ELSIF (instruction = "0000001")THEN -- BGEZ Branch on greater than or equal to zero rt = 00001
			ELSIF (instruction = "000110") THEN -- BLEZ Branch on less than or equal to zero
			ELSIF (instruction = "000111") THEN -- BGTZ Branch on greater that zero 
			ELSIF (instruction = "000010") THEN  -- Jump 
			ELSIF (instruction = "000001") THEN -- JAL
			ELSIF (instruction = "000000") THEN -- JALR
			
			ELSIF (instruction = "100000" ) THEN -- LB Load Byte 
			ELSIF (instruction = "100001" ) THEN --LH Load HalfWord
			ELSIF (instruction = "101000" ) THEN -- SB store byte
			ELSIF (instruction = "101001" ) THEN -- SH store half
			ELSIF (instruction = "100100" ) THEN -- LBU load byte unsigned
			ELSIF (instruction = "100101" ) THEN -- LHU load half unsigned
			ELSE 						--OTHER		-- Possibly for I-type instructions (the uncommented stuff)
				ALUSrc 		<= '1';
				MemToReg	<= '0';
				RegDst		<= '0';
				RegWrite	<= '1';
				Branch		<= '0';
				MemWrite	<= '0';
				ALUOP0 		<= '0';
				ALUOP1		<= '0';
				MemRead		<= '0';
				JumpMode	<= '';
			END IF;
		END PROCESS;

END beh;		