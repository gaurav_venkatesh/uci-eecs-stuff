library ieee;
USE ieee.std_logic_1164.ALL; 

ENTITY And_Gate IS
PORT(
		in0 : std_logic_vector(31 downto 0 );
		in1 : std_logic_vector(31 downto 0 );
		out1: std_logic_vector(31 downto 0 );
);
END And_Gate;

ARCHITECTURE ander OF And_Gate IS
BEGIN
	
	out1 <= in0 and in1;
END